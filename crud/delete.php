<?php
include_once "../logic/db.php";

$db = new db();
foreach ($_POST['sku'] as $sku){
    //Clears tables which are connected to main table
    $db->delete('book', $sku);
    $db->delete('furniture', $sku);
    $db->delete('dvd', $sku);
    //Clears main table
    $db->delete('product', $sku);
}

header("Location: ../views/list.php");
