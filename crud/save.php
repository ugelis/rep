<?php
include_once "../logic/db.php";
include_once "../logic/saveController.php";



$db = new db();
//Checks if SKU is already in database
if ($db->skuExists())
{
    // Alert doesn't work if JQ logic is in different file;
   echo "<script type='text/javascript'>alert('Product with this SKU already exists');window.location.href='../views/add.php';</script>";
}
else{
    $save = new saveController();
    $save->Save();
    header("Location: ../views/list.php");
}


