<?php
include_once "querys.php";

class db
{
    private $conn;

    public function __construct($dbhost = 'localhost', $dbuser = 'root', $dbpass = 'admin', $db = 'products')
    {
        $this->conn = new mysqli($dbhost, $dbuser, $dbpass, $db) or die("Connect failed: %s\n" . $this->conn->error);
    }

    public function closeCon()
    {
        $this->conn->close();
    }

    public function query($query)
    {
        return mysqli_query($this->conn, $query);
    }

    public function fechAssoc($res)
    {
        return mysqli_fetch_assoc($res);
    }

    public function rowCount($res)
    {
        return mysqli_num_rows($res);
    }

    public function getProductsData(){
        $query = new querys();
        return $this->query($query->connectAllDataQuery());
    }

    public function skuExists()
    {
        $query = new querys();
        $result = $this->query($query->selectProductQuery($_POST['SKU']));

        if($this->rowCount($result) != 0)
            return true;
        else
            return false;
    }

    public function isEmpty()
    {
        $query = new querys();

        $result = $this->query($query->getAllProducts());

        if($this->rowCount($result) == 0)
            return true;
        else
            return false;
    }

    //Types:  'book'/'furniture'/'dvd'
    public function delete($type, $SKU){
        $query = new querys();
        $this->query($query->deleteQuery($type, $SKU));
    }

}
