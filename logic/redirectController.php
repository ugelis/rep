<?php
include_once "db.php";

class redirectController
{
    public function __construct()
    {
    }

    public function redirect(){
        $db = new db();


        //Checks if there are data to display
        if ($db->isEmpty()){
            header('Location: views/add.php');
        }
        else{
            header("Location: views/list.php");
        }
    }
}