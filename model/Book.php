<?php
include_once "Product.php";
include_once "../logic/db.php";
include_once "../logic/querys.php";

class Book extends Product
{
    private $Weight;

    public function __construct( )
    {
        $this->setSKU($_POST['SKU']);
        $this->setName($_POST['Name']);
        $this->setPrice($_POST['Price']);
        $this->setWeight($_POST['Weight']);
        $this->setType($_POST['type']);
    }


    public function setWeight($Weight)
    {
        $this->Weight = $Weight;
    }

    public function getWeight()
    {
        return $this->Weight;
    }

    public function  save()
    {
        $db = new db();
        $query = new querys();

        $weight = $this->getWeight();
        $name = $this->getName();
        $sku = $this-> getSKU();
        $price = $this-> getPrice();
        $typ = $this-> getType();

        $db->query($query->saveProductQuery($sku, $name, $price, $typ));
        $db->query($query->saveBookQuery($sku, $weight));

        $db->closeCon();
    }

}