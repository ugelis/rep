<?php
include_once "Product.php";
include_once "../logic/db.php";
include_once "../logic/querys.php";

class DVD extends Product
{
    private $Size;

    public function __construct()
    {
        $this->setSKU($_POST['SKU']);
        $this->setName($_POST['Name']);
        $this->setPrice($_POST['Price']);
        $this->setSize($_POST['Size']);
        $this->setType($_POST['type']);
    }

    public function setSize($Size)
    {
        $this->Size = $Size;
    }

    public function getSize()
    {
        return $this->Size;
    }

    public function save()
    {
        $db = new db();
        $query = new querys();

        $sku = $this->getSKU();
        $name = $this->getName();
        $price = $this->getPrice();;
        $size = $this->getSize();
        $typ = $this->getType();

        $db->query($query->saveProductQuery($sku, $name, $price, $typ));
        $db->query($query->saveDVDQuery($sku, $size));
        $db->closeCon();

    }


}