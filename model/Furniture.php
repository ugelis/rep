<?php
include_once "../model/Product.php";
include_once "../logic/db.php";
include_once "../logic/querys.php";

class Furniture extends Product
{
    
    private $HxWxL;

    public function __construct()
    {
        $this->setSKU($_POST['SKU']);
        $this->setName($_POST['Name']);
        $this->setPrice($_POST['Price']);
        $this->setType($_POST['type']);
        $this->setDimensions($_POST['Height'],$_POST['Width'], $_POST['Length']);
    }

    public function setDimensions($H, $W, $L)
    {
        $this->HxWxL = $H . 'x' . $W . 'x' . $L;
    }

    public function getDimensions()
    {
        return $this->HxWxL;
    }

    public function save()
    {
        $db = new db();
        $query = new querys();

        $sku = $this->getSKU();
        $name = $this->getName();
        $price = $this->getPrice();
        $Dimensions = $this->getDimensions();
        $type = $this->getType();

        $db->query($query->saveProductQuery($sku, $name, $price, $type));
        $db->query($query->saveFurnitureQuery($sku, $Dimensions));

        $db->closeCon();

    }

}