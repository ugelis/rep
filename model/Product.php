<?php

 abstract class Product
{
    private $SKU;
    private $name;
    private $price;
    private $type;

    public function setSKU($SKU)
    {
        $this->SKU = $SKU;
    }
    public function setName($name)
    {
        $this->name = $name;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }
    public function setType($type)
    {
        $this->type =$type;
    }

    public function getType()
    {
        return $this->type;
    }
    public function getSKU()
    {
        return $this->SKU;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }

    public function save(){}

}