<!DOCTYPE html>
<html>

<head>
    <link type="text/css" rel="stylesheet" href="style.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <div class="container mt-3">
        <div class="row">
            <div class="col-sm">
                <h1>Product add!</h1>
            </div>
            <div class="btn">
                <button class="safe" type="submit" form="form" name="submit">save</button>
                <button class="danger" id="CancelBTN" href="/add.php">cancel</button>
            </div>
        </div>
    </div>
    <hr>
</head>
<body>
<div class="container center-screen">
    <form method="POST" action="../crud/save.php" id='form'>
        <div>
            <div>
                <label for="sku">SKU:</label>
            </div>
            <div>
                <input type="text" name="SKU"  pattern="[a-zA-Z]{2}[0-9]{3}" placeholder="AA---" title="2 letters + 3 numbers"  required>
            </div>
        </div>
        <div id="Alert"></div>
        <div>
            <div>
                <label for="name">Name:</label>
            </div>
            <div>
                <input type="text" name="Name" required>
            </div>
        </div>
        <div>
            <div>
                <label for="price">Price:</label>
            </div>
            <div>
                <input step=".01" min=".01" type="number" name="Price" required>
            </div>

        </div>
        <div class="pt-4">
            <label for="ProductType">Type Switcher:</label>
            <select id="ProductType" name="type" required>
                <option value="" selected disabled hidden>Choose</option>
                <option value="Book">Book</option>
                <option value= "DVD">DVD</option>
                <option value="Furniture">Furniture</option>
            </select>
        </div>
        <div id="dynamicForm"></div>
    </form>
</div>
<script type="text/javascript" src="scripts/CancelBTN.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="scripts/FormControl.js"></script>
</body>
</html>

