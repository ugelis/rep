<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link type="text/css" rel="stylesheet" href="style.css"/>
    <title>Page Title</title>

    <div  class="container mt-3">
        <div class="row">
            <div class="col-sm">
                <h1>Product list</h1>
            </div>
            <div  class="btn">
                <button  class="safe" id="AddBTN">Add new!</button>
                <button class="danger" type="submit" form="delBTN" name="delete">Dell!</button>

            </div>
        </div>
    </div>
    <hr>
</head>

<body>
<div class="center-screen" >
    <div >
        <form method="post" id="delBTN" action="../crud/delete.php" >
            <?php include_once '../crud/read.php' ?>
        </form>
    </div>

</div>
<script type="text/javascript" src="scripts/AddBtn.js"></script>
</body>
</html>