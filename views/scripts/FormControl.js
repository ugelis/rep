//for add.php
$('#ProductType').on("change", function () {
    const selected = $('#ProductType').val();
    const dynamicForm = $('#dynamicForm');
    const formFunction = {
        Furniture: function() {
            dynamicForm.html(' <div><label>Height:</label></div> <div><input min="1"  name="Height" type="number" required> </div> <div><label>Width:</label></div> <div><input min="1" name="Width" id="Width" type="number" required></div>  <div><label>Length:</label></div>  <div><input min="1" name="Length" id="Length" type="number" required></div>  <p>Please,\n' +
                'provide dimensions!</p>');
        },
        DVD: function() {
            dynamicForm.html(' <div><label>Size(MB):</label></div> <div><input  min="1" name="Size" type="number" required></div><p>Please, provide size!</p>');
        },
        Book: function() {
            dynamicForm.html('  <div><label>Weight:</label></div>  <div><input min="1" name="Weight" type="number" required></div><p>Please, provide weight!</p>');
        }
    };
    formFunction[selected]();
});

